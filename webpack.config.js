const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackPwaManifestPlugin = require('webpack-pwa-manifest');
const WorkboxWebpackPlugin = require('workbox-webpack-plugin')
const path = require('path');

module.exports = {
   output: {
      filename: 'app.bundle.js',
      publicPath: '/'
   },
   devServer: {
      historyApiFallback: {
        disableDotRule: true
      },
      liveReload: true
   },
   plugins: [
      new HtmlWebpackPlugin({
         template: 'src/index.html'
      }),
      new WebpackPwaManifestPlugin({
         name: 'PETgram - Tu app para tus mascotas',
         short_name: 'PETgram',
         description: 'Con petgram puedes encontrar fotos de animales domesticos de una manera muy facil',
         orientation: 'portrait',
         display: 'standalone',
         start_url: '/',
         scope: '/',
         background_color: '#ffffff',
         theme_color: '#2196f3',
         prefer_related_applications: true,
         icons: [
            {
               src: path.resolve('src/assets/icon.png'),
               sizes: [96, 128, 192, 256, 384, 512], // multiple sizes
               purpose: 'any maskable',
               ios: true,
            }
         ]
      })
   ],
   module: {
      rules: [
         {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
               loader: 'babel-loader',
               options: {
                  presets: ['@babel/preset-env', '@babel/preset-react']
               }
            }
         },
         {
            test: /\.(png|jpe?g|gif)$/i,
            use: [
               {
                  loader: 'file-loader',
               },
            ]
         }
      ]
   }
}