import React, { useEffect } from 'react'
import { ListOfCategories } from './../components/ListOfCategories/index';
import { ListOfPhotoCards } from './../components/ListOfPhotoCards/index';
import { Helmet } from 'react-helmet';



const HomePage = ({id}) => {
   useEffect(() => {
      window.scrollTo(0,0);
   }, [])
   return (
      <>
      <Helmet>
         <title>Petgram - Tu app favorita de animales</title>
      </Helmet>
         <ListOfCategories />
         <ListOfPhotoCards categoryId={id} />
      </>
   )
}

export const Home = React.memo(HomePage, (prevProps, props) => {
   return prevProps.id === props.id
})