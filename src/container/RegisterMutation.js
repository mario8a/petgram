import { gql, useMutation } from '@apollo/client'

const REGISTER = gql`
    mutation signup($input: UserCredentials!){
        signup(input: $input)
    }
`
export const useRegisterMutation = (email,password) => {
   const [registerMutation, {data, loading, error}] = useMutation(REGISTER, {variables: {input: {email,password}}})

   return { registerMutation, data, loading, error }
}