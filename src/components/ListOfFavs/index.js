import React from 'react'
import { Grid, Image, Link } from './styles'
import PropTypes from 'prop-types';

export const ListOfFavs = ({favs}) => {

  const Favorites = () => {
    if(favs.length === 0) {
      return <p>No tienes ningun favorito en tu lista</p>
    } else {
      return <Grid>
      {
        favs.map(fav => <Link key={fav.id} to={`/detail/${fav.id}`}>
          <Image src={fav.src} />
        </Link>
        )
      }
    </Grid>
    }
  }

  return (
    <Favorites />
  )
}



ListOfFavs.propTypes = {
  favs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      src: PropTypes.string.isRequired
    })
  ),
}