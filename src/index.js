import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloClient, ApolloProvider, createHttpLink, InMemoryCache } from "@apollo/client";
import { App } from './App';
import Context from './Context';
import { setContext } from '@apollo/client/link/context';
import { onError } from "@apollo/client/link/error";

const httpLink = createHttpLink({
   uri: 'https://petgram-server-mario8a.vercel.app/graphql',
})

const authLink = setContext((_, { headers }) => {
  const token = window.sessionStorage.getItem('token')
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

const errorMidle = onError(
   ({networkError}) => {
      if(networkError && networkError.result.code === 'invalid_token') {
         sessionStorage.removeItem('token');
         window.location.href = '/'
      }
   }
)

const client = new ApolloClient({
   link: authLink.concat(httpLink),
   error: errorMidle,
   cache: new InMemoryCache(),
 })


ReactDOM.render(
   <Context.Provider value={{isAuth: true}}>
      <ApolloProvider client={client}>
         <App />
      </ApolloProvider> 
   </Context.Provider>
   ,document.getElementById('app'));