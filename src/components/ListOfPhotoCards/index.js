import React from 'react';
import { PhotoCard } from './../PhotoCard/index';
import { useGetPhotos } from './../../hoc/withPhotos';

export const ListOfPhotoCards = ({categoryId}) => {

   const { loading, error, data } = useGetPhotos(categoryId)

   if (loading) return <h2>Loading...</h2>
   if (error) return <h2>error...</h2>

   return (
      <ul>
      {data.photos.map((photo) => (
        <PhotoCard key={photo.id} {...photo} />
      ))}
    </ul>
   )
}