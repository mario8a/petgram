import React from 'react';
import { Link } from '@reach/router';
import PropTypes from 'prop-types';
import { Article, ImageWrapper, Img } from './styles';
import { useNearScreen } from './../../hooks/useNearScreen';
import { FavButton } from './../FavButton/index';
import { useMuationToogleLike } from './../../hooks/useMuationToogleLike';

const DEFAULT_IMAGE = 'https://res.cloudinary.com/midudev/image/upload/w_300/q_80/v1560262103/dogs.png';

export const PhotoCard = ({id, liked, likes = 0, src = DEFAULT_IMAGE}) => {
   const [show, element] = useNearScreen()
   const { mutation, mutationLoading, mutationError } = useMuationToogleLike()

   const handleFavClick = () => {
    mutation({
      variables: {
        input: { id }
      }
    })
  }

   return (
      <Article ref={element}>
         {
            show && 
            <>
                <Link to={`/detail/${id}`}>
                  <ImageWrapper>
                     <Img src={src} alt="" />
                  </ImageWrapper>
               </Link>
               <FavButton liked={liked} likes={likes} onClick={handleFavClick}/>
            </>
         }
        
      </Article>
   )
}

PhotoCard.propTypes = {
   id: PropTypes.string.isRequired,
   liked: PropTypes.bool.isRequired,
   src: PropTypes.string.isRequired,
   likes: function(props, propName, componentName) {
      const propValue = props[propName]

      if(propValue == undefined) {
         return new Error(`${propName} Value must be undefined`)
      }
      if (propValue < 0) {
         return new Error(`${propName} value must be greater than 0`)
      }
   }
}