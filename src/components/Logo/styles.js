import styled from "styled-components";

export const Svg = styled.svg`
   width: 100%;
   height: 4rem;
`;