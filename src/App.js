import React, { useContext, Suspense } from 'react';
import { Redirect, Router } from '@reach/router';
import { GlobalStyles } from './styles/GlobalStyles';
import { Logo } from './components/Logo/index';
import { Home } from './pages/Home';
import { NavBar } from './components/NavBar';
import { Detail } from './pages/Detail';
// import { Favs } from './pages/Favs';
import { User } from './pages/User';
import { NotRegisterUser } from './pages/NotRegisterUser';
import { Context } from './Context';
import { NotFound } from './pages/NotFound';

const Favs = React.lazy(() => import('./pages/Favs'));


export const App = () => {
   const { isAuth } = useContext(Context);
   return (
      <Suspense fallback={ <div/> }>
         <GlobalStyles />
         <Logo />
         <Router>
            <NotFound default />
            <Home path="/" />
            <Home path="/pet/:id" />
            <Detail path="/detail/:detailId" />
            {!isAuth && <NotRegisterUser path="/login" /> }
            {!isAuth && <Redirect noThrow  from="/favs" to="/login"/> }
            {!isAuth && <Redirect noThrow  from="/user" to="/login"/> }
            {isAuth && <Redirect from="/login" to="/" />}
            <Favs path="/favs" />
            <User path="/user" />
         </Router>
         <NavBar />  
      </Suspense>
   )
}