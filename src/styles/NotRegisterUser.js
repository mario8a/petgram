import styled from "styled-components";


export const Img = styled.img`
   width: 250px;
   height: 250px;
`;

export const Div = styled.div`
   width: 100%;
   text-align: center;
   p {
      font-size: 15px;
   }

   span {
      color: blueviolet;
      font-weight: 700;
   }
`;