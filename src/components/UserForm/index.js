import React from "react";
import { useInputValue } from "../../hooks/useInputValue";
import { Error, Form, Input, Title } from "./styles";
import { SubmitButton } from './../SubmitButton/index';

export const UserForm = ({ disabled, error, onSubmit, title }) => {
  const email = useInputValue("");
  const password = useInputValue("");

  const handleSubmit = (e) => {
     e.preventDefault();
     onSubmit({
         email: email.value, 
         password: password.value
      })
  }

  return (
    <>
      <Title>{title}</Title>
      <Form disabled={disabled} onSubmit={handleSubmit}>
        <Input disabled={disabled} type="text" placeholder="Email" {...email} />
        <Input disabled={disabled} type="password" placeholder="Password" {...password} />
        <SubmitButton disabled={disabled} >{title}</SubmitButton>
      </Form>
      { error && <Error>{error}</Error> }
    </>
  );
};

// value={password.value}
//         onChange={password.onChange} --> {...password} - Usa todas las propiedades que vienen en password
