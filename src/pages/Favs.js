import React from 'react';
import { FavsWithQuery } from './../container/GetFavorites';
import { Helmet } from 'react-helmet';
import { Layout } from './../components/Layout/index';

export default () => {
   return (
      <Layout title="Tus favoritos">
         <FavsWithQuery />
      </Layout>
   )
}