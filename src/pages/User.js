import React, { useContext } from 'react';
import { Context } from '../Context';
import { SubmitButton} from './../components/SubmitButton/index';
import gatito from '../assets/ben2.png';
import { Img, Div } from './../styles/NotRegisterUser';

export const User = () => {
   const {removeAuth} = useContext(Context)
   return (
      <Div>
         <Img src={gatito} alt="" />
         <SubmitButton onClick={removeAuth}>Cerrar sesion</SubmitButton>
      </Div>
   )
}
