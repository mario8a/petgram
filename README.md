# Petgram


_Proyecto desarrollado en el curso de react avanzado de platzi. Usando una API local se muestran en la aplicacion un clon de instagram enfocado a mascotas. Podemos ver diferentes categorias, dar like, registarnos e iniciar sesion. Los likes quedan guardados de manera temporal los cuales podemos ver dentro de favoritos._

![Captura de Petgram](./.readme-static/Petgram.jpg)
![Captura de Petgram](./.readme-static/petgram2.jpg)

## Ve la aplicacion en tu navegador o celular :)
[Ver aplicacion](https://petgram-mario8a.vercel.app/)

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node js
```

### Instalación 🔧

_Clona el repositorio y una vez clonado, instalá las dependencias necesarias ejecutando el siguiente comando:_

```
npm install
```

_Levanta el proyecto con el siguiente comando:_

```
npm run start dev
```

## Construido con 🛠️

* [React](https://es.reactjs.org/) - El framework web usado
* [webpack](https://webpack.js.org/) - Herramienta usada para el desarrollo de la aplicacion
* [vercel](https://vercel.com/dashboard) - Herramienta usada para eldespliegue de la aplicacion

_Siguientes pasos para esta aplicacion:_

* Crear su service worker para hacerla una PWA

---
⌨️ con ❤️ por [Mario Ochoa](https://www.instagram.com/mario_8a_/) 😊
