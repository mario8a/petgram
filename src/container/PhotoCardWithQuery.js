import React from 'react'
import { PhotoCard } from './../components/PhotoCard/index';
import { gql, useQuery } from '@apollo/client';

const GET_SINPLE_PHOTO = gql`
 query getSinglePhoto($id: ID!) {
    photo(id: $id) {
      id
      categoryId
      src
      likes
      liked
      userId
    }
  }
`

export const PhotoCardWithQuery = ({id}) => {
   // En data viene todo el objeto de la peticion al servidor
   const { loading, error, data } = useQuery(GET_SINPLE_PHOTO, {
      variables: {
        id: id
      }
    });

    if (error) <h2>Internal Server Error</h2>
    if (loading) return <h2>Loading...</h2>

   return (
      <PhotoCard {...data.photo}/>
   )
}
