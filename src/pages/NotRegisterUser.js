import React, { useContext, useState } from 'react';
import { Context } from '../Context'
import { UserForm } from './../components/UserForm/index';
import { useRegisterMutation } from './../container/RegisterMutation';
import { useLoginMutation } from './../container/LoginMutation';
import { Img, Div } from '../styles/NotRegisterUser';

import logo from '../assets/icon.png';

export const NotRegisterUser = () => {
   const {activateAuth} = useContext(Context);
   const { registerMutation, data: dataReg, error: errorReg, loading: loadingReg } = useRegisterMutation()
   const { loginMutation, data: dataLog, error: errorLog, loading: loadingLog } = useLoginMutation();
   const [showRegisterForm, setshowRegisterForm] = useState(false)


   const registerSubmit = ({email, password}) => {
      const input = { email, password }
      const variables = { input }
      registerMutation({ variables })
      .then(({data}) => {
         const {signup} = data;
         activateAuth(signup)
      })
   }
   const errorMsgReg  = errorReg && 'El usuario ya existe o hay algun problema';

   const loginSubmit = ({email, password}) => {
      const input = { email, password }
      const variables = { input }
      loginMutation({ variables })
      .then(({data}) => {
         const {login} = data;
         activateAuth(login)
      })
   }
   const errorMsgLog = errorLog && 'La contraseña no es correcta o el usuario no existe';
   
   return (
      <>
      {
         showRegisterForm
         ? 
         (
         <Div>
            <Img className="logo" src={logo} alt="" />
            <UserForm disabled={loadingReg} error={errorMsgReg} onSubmit={registerSubmit} title='Registrarse'/>
            <>
               <p>¿Ya tienes una cuenta? <span onClick={() => setshowRegisterForm(false)}>Iniciar sesion</span></p>
            </>
         </Div>
         ) 
         :
         (
         <Div>
            <Img className="logo" src={logo} alt="" />
            <UserForm disabled={loadingLog} error={errorMsgLog} onSubmit={loginSubmit} title='Iniciar sesion'/>
            <p>¿No tienes una cuenta? <span onClick={() => setshowRegisterForm(true)}>Registar</span> </p>
         </Div>
         )
      }
      </>
   )
}
