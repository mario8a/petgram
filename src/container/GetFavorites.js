import React from 'react';
import { gql, useQuery } from '@apollo/client';
import { ListOfFavs } from '../components/ListOfFavs';

const GET_FAVORITES = gql`
query  getFavs {
    favs {
      id
      categoryId
      src
      likes
      userId
    }
  }
`;


const useGetFavorites = () => {
  const { data, error, loading } = useQuery(GET_FAVORITES, { fetchPolicy: 'cache-and-network' });
  return { data, loading, error };
};

export const FavsWithQuery = () => {
   const {data, loading, error} = useGetFavorites();
   if(loading) return <p>Loading...</p>
   if(error) return <p>Error...</p>
   const {favs} = data;

   return <ListOfFavs favs={favs} />
}